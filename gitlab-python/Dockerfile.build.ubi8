ARG BUILD_IMAGE=

FROM ${BUILD_IMAGE}

ARG PYTHON_VERSION=3.9.17

ENV LANG=C.UTF-8

RUN mkdir /assets \
    && curl -f --retry 6 -L https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tgz -o Python-${PYTHON_VERSION}.tgz \
    && curl -f --retry 6 -L https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tgz.asc -o Python-${PYTHON_VERSION}.tgz.asc \
    && gpg2 --keyserver hkps://keys.openpgp.org --auto-key-retrieve --verify Python-${PYTHON_VERSION}.tgz.asc Python-${PYTHON_VERSION}.tgz \
    && tar -xzf Python-${PYTHON_VERSION}.tgz \
    && cd Python-${PYTHON_VERSION} \
    && ./configure --prefix=/usr/local --enable-shared --with-readline=editline LDFLAGS='-Wl,--rpath=/usr/local/lib' \
    && make -j "$(nproc)" \
    && make -j "$(nproc)" install \
    && ldconfig \
    && pip3 install --upgrade pip setuptools \
    && rm -rf /usr/local/lib/python3.9/test \
    && find /usr/local/lib/python3.9 -name '__pycache__' -type d -exec rm -r {} + \
    && cp -R  --parents /usr/local/{bin,lib,include} /assets
